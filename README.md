**Hello**

This is code used in Janis Oskars Darčiks magister work.
There are four different robot navigation problems solved in Python:

    - RRT planner
    - Coverage Planner
    - Obstacle avoidance
    - Point following with mobile robot forward and inverse kinematics

See videos bellow to understand these problems:

Coverage Planner test
https://youtu.be/3nMKmmk9U-8

Kinematic model comparison in Solidworks
https://youtu.be/TFGwKMn3CyM

Differential drive robot without point following
https://youtu.be/na8uGii-y50

Differential drive robot with point following
https://youtu.be/vEKJr0iNTnY

________________________________________________________________________________

**Sveiki**

Šis kods ir izmantots Jāņa Oskara Darčika maģistra darbā.
Tiek ieviesti četri dažādas robotu vadības risinājumi Python vidē:

     - RRT plānotājs;
     - Pārklājuma plānotājs;
     - Izvairīšanās no šķēršļiem;
     - Punkta sekošana ar mobilo robotu uz priekšu un apgrieztā kinemātika.

Lai iepazītu šos risinājumus, skatīties videoklipus zemāk:

Pārklājuma plānotāja pārbaude
https://youtu.be/3nMKmmk9U-8

Kinemātisko modeļu salīdzinājums Solidworks vidē
https://youtu.be/TFGwKMn3CyM

Diferenciālās piedziņas robots bez sekošanas punktam
https://youtu.be/na8uGii-y50

Diferenciālās piedziņas robots ar sekošanu punktam
https://youtu.be/vEKJr0iNTnY
