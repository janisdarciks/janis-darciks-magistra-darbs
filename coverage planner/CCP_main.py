from modules.get_data import get_data
from modules.get_data_list import get_data_list
from modules.ccp import CCP
import random
import numpy as np


def main():
    MAP_COUNT = 1

    robot_size = (60, 40)
    cleaned_percentage_list = []
    path_len_list = []
    all_free_nodes_list = []
    cleaned_nodes_list = []
    total_driven_clean_list = []
    for i in range(MAP_COUNT):
        # Read data from planner
        filename = f'maps//map{i}.png'
        # filename = 'map1.png'
        map_data = get_data(filename)
        map_data['data'] = get_data_list(map_data)

        print("start " + __file__)
        while True:
            random_start = [random.randint(20 + robot_size[0] + 2, 600 - robot_size[0] - 2),
                            random.randint(20 + robot_size[1] + 2, 600 - robot_size[1] - 2)]
            cover = CCP(start=random_start, map_data=map_data, rob_size=robot_size)
            path, cleaned_percentage, all_free_nodes, cleaned_nodes, total_driven_clean = cover.planning()
            if path is None:
                continue
            elif path is False:
                print(f"In map {i} there was an unreachable part!")
                break
            else:
                all_free_nodes_list.append(all_free_nodes)
                cleaned_nodes_list.append(cleaned_nodes)
                total_driven_clean_list.append(total_driven_clean)
                path_len_list.append(len(path))
                cleaned_percentage_list.append(cleaned_percentage)
                print(f"Path {i} is made!")
                break
    cleaned_percentage_list = np.array(cleaned_percentage_list)
    avg_cleaned_percentage = np.average(cleaned_percentage_list)
    print(f"Average cleaned percentage of {cleaned_percentage_list.size} maps is {round(avg_cleaned_percentage, 2)}.")

    all_free_nodes_list = np.array(all_free_nodes_list)
    avg_all_free_nodes = np.average(all_free_nodes_list)
    print(f"Averagely there was {round(avg_all_free_nodes, 0)} nodes to visit.")

    cleaned_nodes_list = np.array(cleaned_nodes_list)
    cleaned_nodes_percentage_list = cleaned_nodes_list / all_free_nodes_list
    avg_cleaned_nodes_percentage = np.average(cleaned_nodes_percentage_list) * 100
    print(f"Average percent of cleaned nodes is {round(avg_cleaned_nodes_percentage, 2)}.")

    total_driven_clean_list = np.array(total_driven_clean_list)
    avg_total_driven_clean = np.average(total_driven_clean_list)
    print(f"Average redriven clean node count is {round(avg_total_driven_clean, 0)}.")


if __name__ == '__main__':
    main()
