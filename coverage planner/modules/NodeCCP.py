class Node:
    def __init__(self, x, y, x_virtual, y_virtual, free):
        self.x = x
        self.y = y
        self.x_virtual = x_virtual
        self.y_virtual = y_virtual
        self.free = free
        self.direction = None