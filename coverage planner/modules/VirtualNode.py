class VirtualNode:
    def __init__(self, x, y, free):
        self.x = x
        self.y = y
        self.free = free
        self.dirty = True
        self.direction = None