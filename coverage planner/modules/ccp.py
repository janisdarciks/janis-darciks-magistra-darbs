import copy
import numpy as np
import matplotlib.pyplot as plt
from modules.NodeCCP import Node
from modules.VirtualNode import VirtualNode
import matplotlib
matplotlib.use('QT4Agg')
import time


class CCP:
    """
    Complete Coverage Path Planning
    """

    def __init__(self, start, map_data, rob_size, STOP_COEF=0.99):
        # width must be larger than actual to avoid walls!
        self.start = start
        self.data_list = np.array(map_data['data'])
        # self.obstacle_data_list = np.array(self.data_list)
        # self.data_list = map_data['data']
        self.area_size = [0, map_data['size'][0] - 1, 0, map_data['size'][1] - 1]
        self.rob_size = rob_size  # length x width (x * y) (must not be odd)
        self.robot_move_error_margin = 2
        self.STOP_COEF = STOP_COEF
        self.cleaned_nodes = 0
        self.compass = {'N': 0,
                        'E': 1,
                        'S': 2,
                        'W': 3}
        self.compass_cases = {0: 'N',
                              1: 'E',
                              2: 'S',
                              3: 'W'}
        self.BACKWARD_TO_STOP = 1

    def check_start(self):
        start_area = self.data_list[self.start[1] - self.robot_move_error_margin - self.rob_size[1]:
                                    self.start[1] + self.robot_move_error_margin + self.rob_size[1],
                     self.start[0] - self.robot_move_error_margin - self.rob_size[0]:
                     self.start[0] + self.robot_move_error_margin + self.rob_size[0]]
        start_area = np.array(start_area)
        if np.all(start_area):
            return True
        else:
            return False

    def __fill_obstacle_map(self):
        # plt.close()
        # fig, ax = plt.subplots()
        # ax.imshow(self.data_list, cmap='gray')  # shows map
        # plt.gca().invert_yaxis()  # inverts map
        # plt.tick_params(labelsize=20)
        # plt.show()
        # create robot width circle
        # t1 = time.time()
        radius = int(self.rob_size[1] / 2 + self.robot_move_error_margin)
        size = radius * 2 + 1
        rotation_mask = np.zeros((size, size), dtype=bool)
        y, x = np.ogrid[-radius:size - radius, -radius:size - radius]
        mask = x * x + y * y <= radius * radius
        rotation_mask[mask] = True

        map_size = self.data_list.shape
        unreachable_map = np.zeros(map_size)
        for i in range(radius + 1, map_size[0] - radius, 1):
            for j in range(radius + 1, map_size[1] - radius, 1):
                map_part = self.data_list[i - radius - 1:i + radius, j - radius - 1:j + radius]
                not_collision = np.all(map_part)
                if not_collision:
                    unreachable_map[i - radius - 1:i + radius, j - radius - 1:j + radius] = np.logical_or(
                        unreachable_map[i - radius - 1:i + radius, j - radius - 1:j + radius], rotation_mask)
                    # unreachable_map[i - radius - 1:i + radius, j - radius - 1:j + radius] = rotation_mask

        self.data_list = unreachable_map
        # t2 = time.time()
        # t = t2 - t1
        # print(f'Time {t}')
        # # Display the map
        # plt.close()
        # fig, ax = plt.subplots()
        # ax.imshow(self.data_list, cmap='gray')  # shows map
        # plt.gca().invert_yaxis()  # inverts map
        # plt.tick_params(labelsize=20)
        # plt.show()

    def calculate_cleaned_percentage(self):
        sum_of_all = np.sum(self.data_list)
        sum_of_clean = round(self.node_size[0]) * round(self.node_size[1]) * self.cleaned_nodes
        cleaned_space_percents = sum_of_clean / sum_of_all * 100
        return cleaned_space_percents

    def get_visitable_node_list(self):
        real_node_line = []
        real_node_list = []
        virtual_node_line = []
        virtual_node_list = []
        x = 0
        y = 0
        area_size = [self.area_size[1] + 1, self.area_size[3] + 1]
        max_x = area_size[0]
        max_y = area_size[1]

        # Define virtual size (how many nodes in width and height)
        # self.rob_size[1] --> width
        new_size = [max_x / self.rob_size[1], max_y / self.rob_size[1]]  # x, y
        if new_size[0] % 1.0:
            new_size[0] = new_size[0] + 1
        if new_size[1] % 1.0:
            new_size[1] = new_size[1] + 1
        new_size[0], new_size[1] = int(new_size[0]), int(new_size[1])

        # Recalculates node size after reshaping
        counter_x = area_size[0] / new_size[0]
        counter_y = area_size[1] / new_size[1]

        for i in range(new_size[1]):
            for j in range(new_size[0]):
                # Looks if any pixel in node is taken
                subarray = self.data_list[y: y + round(counter_x) + 1, x: x + round(counter_y) + 1]
                subarray.flatten()

                # Creates real and virtual nodes and tags them as free or taken (if any pixel is black)
                if np.all(subarray):  # Checks if all values are true
                    value1 = [(j + 1 / 2) * counter_x, (i + 1 / 2) * counter_y, True]
                    value2 = VirtualNode(x=j, y=i, free=True)
                else:  # if any is not true
                    value1 = [(j + 1 / 2) * counter_x, (i + 1 / 2) * counter_y, False]
                    value2 = VirtualNode(x=j, y=i, free=False)
                real_node_line.append(value1)
                virtual_node_line.append(value2)
                x += round(counter_x)

            real_node_list.append(np.array(real_node_line))
            virtual_node_list.append(np.array(virtual_node_line))
            real_node_line = []
            virtual_node_line = []
            x = 0
            y += round(counter_y)
            node_size = (counter_x, counter_y)

        return np.array(real_node_list), np.array(virtual_node_list), node_size

    # Firstly drives from starting position to closest node
    def find_first_node(self):
        # Looks for node in one nodes size in each direction
        xrange = (self.start[0] - self.node_size[0], self.start[0] + self.node_size[0])  # lower limit, upper limit
        yrange = (self.start[1] - self.node_size[1], self.start[1] + self.node_size[1])

        chosen_sample = None
        for i, line in enumerate(self.real_node_list):
            for j, sample in enumerate(line):
                # Takes first found node in defined range
                if sample[0] > xrange[0] and sample[0] < xrange[1] and sample[1] > yrange[0] and sample[1] < yrange[1]:
                    if sample[2] :
                        chosen_sample = sample
                        break
                    else:
                        continue
            if chosen_sample is not None:
                break
        chosen_sample = Node(x=chosen_sample[0],
                             y=chosen_sample[1],
                             x_virtual=j,
                             y_virtual=i,
                             free=chosen_sample[2])

        # Calculates distance from start to chosen node in x and y axis
        x = self.start[0] - chosen_sample.x
        y = self.start[1] - chosen_sample.y

        # Takes closes looking direction from starting point to first node
        if x >= y:
            if x >= 0:
                start_direction = 'W'
            else:
                start_direction = 'E'
        else:
            if y >= 0:
                start_direction = 'S'
            else:
                start_direction = 'N'

        chosen_sample.direction = start_direction

        return chosen_sample

    def planning(self):
        if not self.check_start():
            print('Start generated in obstacle!')
            return None, None, None, None, None

        self.__fill_obstacle_map()
        self.real_node_list, self.virtual_node_list, self.node_size = self.get_visitable_node_list()  # dims - y, x

        path = [self.start]
        first_node = self.find_first_node()
        node = copy.deepcopy(first_node)
        self.virtual_node_list[node.y_virtual][node.x_virtual].dirty = False
        # self.virtual_node_list[node.y][node.x].dirty = False
        # node.dirty = False
        path.append([node.x, node.y])
        node_list = [node]
        clean_count = 0  # count driven clean in a row
        total_driven_clean = 0
        cleaned_nodes = 1
        last_chosen_clean = False
        backward_count = 0

        # Counts how many free nodes are there
        all_free_nodes = 0
        for line in self.virtual_node_list:
            for sample in line:
                if sample.free is True:  # or sample.dirty is False:
                    all_free_nodes += 1

        while True:
            real_next_node, virtual_next_node, new_direction, chosen_clean = \
                self.find_next_node(node, node_list, last_chosen_clean)
            if not real_next_node:
                print('Out of path!')
                return None, None, None, None, None

            # Create new node
            node = copy.deepcopy(node)  # new_node
            node.x = real_next_node[0]
            node.y = real_next_node[1]
            node.x_virtual = virtual_next_node[0]
            node.y_virtual = virtual_next_node[1]
            node.direction = new_direction

            # Counts how many times drives again trough already cleaned nodes
            if self.virtual_node_list[node.y_virtual][node.x_virtual].dirty is False:
                clean_count += 1
                total_driven_clean += 1
            else:
                clean_count = 0

            # If have driven trough already cleaned nodes x times in row check for end condition
            if node_list == []:
                return False, False, False, False, False

            if backward_count > self.BACKWARD_TO_STOP:  # 160:
                # break

                # If cleaned at least STOP_COEF percent of room stop
                if cleaned_nodes / all_free_nodes >= self.STOP_COEF:
                    break
                clean_count = 0
                # cleaned_nodes = 0

            # Counts how many nodes are clean and tags cleaned as not dirty
            if self.virtual_node_list[node.y_virtual][node.x_virtual].dirty is True:
                self.virtual_node_list[node.y_virtual][node.x_virtual].dirty = False
                cleaned_nodes += 1

            path.append([node.x, node.y])  # path as real coordinates

            # Uses this when goes backwards in cleaned path to find new dirty nodes to clean
            if chosen_clean is False:
                node_list.append(node)
                backward_count = 0
            else:
                backward_count += 1

            last_chosen_clean = chosen_clean
        self.cleaned_nodes = cleaned_nodes

        # To see results
        print(f'paths length: {len(path)}')
        print(f'total free points: {all_free_nodes}')
        print(f'total cleaned: {cleaned_nodes}')
        print(f'redriven clean: {total_driven_clean}')

        cleaned_percentage = self.calculate_cleaned_percentage()
        print(f'cleaned percentage: {round(cleaned_percentage, 0)}%')

        # path = self.node_moved_by_obstacle(path)  # pushes path away from obstacles

        # Display the map
        plt.close()
        fig, ax = plt.subplots()
        ax.imshow(self.data_list, cmap='gray')  # shows map
        plt.gca().invert_yaxis()  # inverts map
        plt.tick_params(labelsize=20)

        for row in self.real_node_list:  # shows available nodes
            for node in row:
                if node[2] == True:
                    plt.plot(node[0], node[1], 'bx', markersize=8)

        # ====== To watch each iteration
        last = path[0]
        for sample in path[1:]:
            plt.plot([sample[0], last[0]], [sample[1], last[1]], '-r', linewidth=6.0)
            last = sample
            plt.pause(0.1)

        # ====== To see whole image
        # plt.plot([x for (x, y) in path], [y for (x, y) in path], '--r')     # shows path

        # plt.plot([self.start[0], node[0]], [self.start[1], node[1]], '--r')
        plt.pause(1.0)
        plt.show()

        return path, cleaned_percentage, all_free_nodes, cleaned_nodes, total_driven_clean

    def find_next_node(self, node, node_list, last_chosen_clean):

        chosen_value = None
        chosen_clean = False  # Changes if chosen value is clean

        # Finds first direction case
        case = self.compass[node.direction]
        if case == 0:
            case = 4
        case -= 1  # starts with left side from robot

        # For four sides of robot
        for i in range(4):
            new_direction = self.compass_cases[case]

            # Finds next node
            if new_direction == 'N':
                value = self.virtual_node_list[node.y_virtual + 1][node.x_virtual]
                value.direction = new_direction
            elif new_direction == 'E':
                value = self.virtual_node_list[node.y_virtual][node.x_virtual + 1]
                value.direction = new_direction
            elif new_direction == 'S':
                value = self.virtual_node_list[node.y_virtual - 1][node.x_virtual]
                value.direction = new_direction
            elif new_direction == 'W':
                value = self.virtual_node_list[node.y_virtual][node.x_virtual - 1]
                value.direction = new_direction

            # Change direction for next side
            case += 1  # to go clockwise
            if case == 4:
                case = 0

            if value.free is not True:  # checks if no wall
                continue

            # Takes first found dirty node
            if value.dirty is True:
                chosen_value = value
                break

            else:  # if its clean
                if chosen_value is None:  # if first choose this
                    chosen_value = value

        # For case when chosen is dirty
        if chosen_value.dirty is True:
            real_neighbour = ([self.real_node_list[chosen_value.y][chosen_value.x][0],
                               self.real_node_list[chosen_value.y][chosen_value.x][1]])
            virtual_neighbour = ([self.virtual_node_list[chosen_value.y][chosen_value.x].x,
                                  self.virtual_node_list[chosen_value.y][chosen_value.x].y])

        else:  # when chosen is clean, then fallow path backwards
            chosen_clean = True

            # If it is first time in row when robot chooses fallow path backwards, pop two nodes (to change direction
            # and to change placement)
            if last_chosen_clean is False:
                try:
                    chosen_value = node_list.pop()
                    new_direction = chosen_value.direction
                    chosen_value = node_list.pop()
                except:
                    return None, None, None, None   # when out of path
            else:  # if many times in row then only direction must be changed
                try:
                    chosen_value = node_list.pop()
                    new_direction = chosen_value.direction
                except:
                    return None, None, None, None   # when out of path

            # Changes direction as it goes backwards
            if new_direction == 'W':
                new_direction = 'E'
            elif new_direction == 'N':
                new_direction = 'S'
            elif new_direction == 'E':
                new_direction = 'W'
            else:
                new_direction = 'N'

            real_neighbour = ([self.real_node_list[chosen_value.y_virtual][chosen_value.x_virtual][0],
                               self.real_node_list[chosen_value.y_virtual][chosen_value.x_virtual][1]])
            virtual_neighbour = ([self.virtual_node_list[chosen_value.y_virtual][chosen_value.x_virtual].x,
                                  self.virtual_node_list[chosen_value.y_virtual][chosen_value.x_virtual].y])

        return real_neighbour, virtual_neighbour, new_direction, chosen_clean
