import mysql.connector
import numpy as np
from struct import unpack
from PIL import Image


# To get data from database
def get_data(filename):
    img = Image.open(filename).convert('L')

    # img = img.crop((600, 550, 1400, 1150))      # no cropping for img2

    # Makes image black(1) and white(255) and gets list with data
    x_list = []
    y_list = []
    data = []
    for y in range(img.size[1]):
        for x in range(img.size[0]):
            x_list.append(x)
            y_list.append(y)
    value = list(img.getdata())
    data_list = list(zip(x_list, y_list, value))

    for sample in data_list:
        data.append(sample[0])
        data.append(sample[1])
        data.append(sample[2])

    map_data = {
        'size': img.size,
        'data': data
    }

    return map_data

    # db = mysql.connector.connect(
    #     host='com19-misik.ditf.rtu.lv',
    #     user="com19",
    #     passwd='rcBGwR7ObUOjSECA',
    #     database="com19"
    # )
    # rob_id = 'B827EB6B4CEE'
    # parent_id = '000000000001'
    #
    # cursor = db.cursor()
    # cursor.execute(('SELECT data FROM map WHERE robot_id=%s AND env_id=%s'), (rob_id, parent_id))
    # result = cursor.fetchall()
    # cursor.close()
    #
    # map_data = result[0][0]
    # # parse header
    # header_data = map_data[:52]
    # map_data = map_data[52:]
    # temp = unpack('=dIIdIddd', header_data)
    # header = {
    #     'timestamp': temp[0],
    #     'width': temp[1],
    #     'height': temp[2],
    #     'scale': temp[3],
    #     'quality': temp[4],
    #     'hole_width': temp[5],
    #     'center_x': temp[6],
    #     'center_y': temp[7]
    # }
    # # parse body
    # map = np.asarray([unpack('=H', map_data[idx:idx + 2])[0] for idx in range(0, len(map_data), 2)], dtype=np.float64)
    # map = np.reshape(map, (header['width'], header['height'])).T
    #
    # # Normalize
    # max_value = np.max(map)
    # map = map / max_value
    #
    # map = map.tolist()
    # map_data = {
    #     'header': header,
    #     'data': map
    # }
    #
    # return map_data
