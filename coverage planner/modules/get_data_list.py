import numpy as np

# Converts data [x, y, value, x2, y2, v2, ...] list to ndimensional list with image sized arrays
# def get_data_list(data):
#     for y, line in enumerate(data):
#         for x, sample in enumerate(line):
#             if sample >= 150 / 256:  # 230:
#                 sample = True
#             else:
#                 sample = False
#             data[y][x] = sample
#
#     data = np.array(data)
#
#     return data

def get_data_list(data):
    size = data['size']
    data = data['data']
    # sample = []
    data_line = []
    data_list = []
    for i in range(0, len(data), 3):
        # if i/3 + 1 // size[1]:
        #     # split
        #     pass
        # sample.append(data[i])
        # sample.append(data[i + 1])

        if data[i + 2] >= 230:       # 230:
            value = True
        else:
            value = False

        if i == 0:
            # sample.append(value)
            data_line.append(value)
            # data_list.append(sample)
            # sample = []
            continue
        if data[i] < data[i - 3]:
            data_list.append(data_line)
            data_line = []

        # sample.append(value)
        data_line.append(value)
        # data_list.append(sample)
        # sample = []

    return data_list
