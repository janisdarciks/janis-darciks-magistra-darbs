import numpy as np
import copy
from modules.forward import forward
from modules.inverse import inverse
from modules.draw import *
from modules.find_wheel_param import find_wheel_center
from modules.cubic_spline import Spline2D
# import matplotlib
# matplotlib.use('TkAgg')


# map = np.ones((10, 10))
# map[0, 0] = 0
# draw_map(map, blank=True)

start = np.asarray([1, 0, 0])
dist_to_wheels = [0.70710678118654752440084436210485, 0.70710678118654752440084436210485]
alpha = [np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4]
beta = np.asarray([- np.pi/4, - 3*np.pi/4])
enc_const = np.asarray([1e-6, 1e-6])
t = 0.05
# last_point = np.asarray(start)
preferable_speed = 0.2
DIST_TO_GOAL = 0.015
DIST_POINT_TO = 0.05


#    .
# O     O
# a = forward(encoders=[np.pi/4, -np.pi/4], dist_to_wheels=[7.0710678118654752440084436210485, 7.0710678118654752440084436210485],
#             alpha=[np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4], beta=[- np.pi/4, - 3*np.pi/4],
#             enc_const=[10, 10],
#             last_point=[10, 5, 0], t=1)
# print(a)


trajectory = [[2, 2], [4, 2], [4, 4], [6, 6], [6, 4], [5, 4], [6, 2]]
point_listx = [start[0]]
point_listy = [start[1]]
for i in range(len(trajectory)):
    point_listx.append(trajectory[i][0])
    point_listy.append(trajectory[i][1])

# Generate very begining for spline, in wheel center
wheel_center = find_wheel_center(start, dist_to_wheels, alpha)

# Generate ending point close to very last one
ending_point = [trajectory[-1][0] + 1e-5, trajectory[-1][1] + 1e-5]
trajectory.append(ending_point)

# Generate smooth path using spline
point_listx.append(ending_point[0])
point_listy.append(ending_point[1])
sp = Spline2D(point_listx, point_listy)
s = np.arange(0, sp.s[-1], 0.3)
trajectory = []
tmp_list = []
for i_s in s:
    ix, iy = sp.calc_position(i_s)
    tmp_list.append(ix)
    tmp_list.append(iy)
    trajectory.append(tmp_list)
    tmp_list = []
trajectory.pop(0)
trajectory.append([6, 2])
trajectory.append(ending_point)

point_listx = []
point_listy = []
for i in range(len(trajectory)):
    point_listx.append(trajectory[i][0])
    point_listy.append(trajectory[i][1])

encoder_list = []
new_trajectory = []
wheel_coord_list = []
velocity_list = []
velocity_list_f = []
omega_list = []

last_point = wheel_center
point_from = start
copy_point_from = copy.deepcopy(point_from)
point_to = trajectory.pop(0)
next_point = trajectory.pop(0)

while True:
    # Find encoder values to aim to point
    encoders = inverse(preferable_speed, dist_to_wheels, alpha, beta, enc_const, point_from, point_to, t)
    encoder_list.append(encoders)

    # Find where vehicle would go with found encoder values
    point_f, velocity_f = forward(encoders, dist_to_wheels, alpha, beta, enc_const, point_from, t)
    point_f = point_f.flatten()

    # Check dist to point
    # TODO needs while loop for finding closest point on trajectory and drop ones
    dist = np.sqrt(np.power(point_to[0] - point_f[0], 2) + np.power(point_to[1] - point_f[1], 2))
    if dist < DIST_POINT_TO:
        if trajectory:  # if list not empty (last point)
            # Case 1: change last point when when point to ie reached
            last_point = copy_point_from
            copy_point_from = copy.deepcopy(point_from)
            point_to = next_point
            next_point = trajectory.pop(0)

        # Finishing
        else:
            if dist < DIST_TO_GOAL:     # Finished
                new_trajectory.append(point_f)
                break
            else:   # Some last iterations
                pass
    else:
        if trajectory:
            while True:
                dist2 = np.sqrt(np.power(next_point[0] - point_f[0], 2) + np.power(point_to[1] - point_f[1], 2))
                if dist < dist2:    # if point_to is closer than next_point to point_f
                    break
                else:               # skip point to and choose next
                    last_point = copy_point_from
                    copy_point_from = copy.deepcopy(point_from)
                    dist = dist2
                    point_to = next_point
                    next_point = trajectory.pop(0)

    new_trajectory.append(point_f)
    # Case 2: change last point each iter
    # last_point = point_from
    point_from = point_f
    # velocity_list.append(moment_velocity)
    velocity_list_f.append(velocity_f)
    # omega_list.append(omega)
    # Case 3: (in inverse) with 3 points and velocity angle from 0

# print(encoder_list)
new_point_listx = [new_trajectory[i][0] for i in range(len(new_trajectory))]
new_point_listy = [new_trajectory[i][1] for i in range(len(new_trajectory))]


plt.subplots(1)
plt.figure(1)
plt.subplot(211)
plt.plot(point_listx, point_listy)
plt.grid()
plt.subplot(212)
plt.plot(new_point_listx, new_point_listy)
plt.grid()
#
# plt.subplots(1)
# plt.figure(2)
# plt.subplot(211)
# plt.plot(velocity_list)
# plt.grid()
# plt.subplot(212)
# plt.plot(omega_list)
# plt.grid()


plt.show()



