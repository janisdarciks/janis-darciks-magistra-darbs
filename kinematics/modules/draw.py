import matplotlib.pyplot as plt


def draw_map(map, blank=False):
    if blank:
        plt.close()
    fig, ax = plt.subplots()
    ax.imshow(map, cmap='gray')  # shows map
    plt.gca().invert_yaxis()  # inverts map
    plt.grid()


def draw_point(point, marker='rx'):
    plt.plot(point[0], point[1], marker)


def draw_line(point1, point2, line='b-'):
    plt.plot([point1[0], point2[0]],
             [point1[1], point2[1]], line)


def show_drawing():
    plt.show()
