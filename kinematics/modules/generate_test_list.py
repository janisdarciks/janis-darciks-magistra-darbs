import numpy as np
from modules.forward import forward
from modules.inverse import inverse
from modules.draw import *
from modules.find_wheel_param import find_wheel_coord
# import matplotlib
# matplotlib.use('TkAgg')


map = np.ones((10, 10))
map[0, 0] = 0
draw_map(map, blank=True)

start = np.asarray([5, 5, 0])    # 0.7853981633974483
dist_to_wheels = [5, 5]
alpha = [np.pi / 2, 0]
beta = np.asarray([0, np.pi/2])
enc_const = np.asarray([10, 10])
t = 1
last_point = np.asarray(start)
preferable_speed = 0.5     # 6.2


# Generate trajectory for inverse--------------------
encoders = []
for i in range(7):
    encoders.append([np.pi/16, np.pi/17])


# Test forward---------------------------------------
trajectory = [start]

# Draw start
draw_point(start, marker='cx')

# Draw points
last_point = start
icr_f_list = []
wheel_coord_list = [find_wheel_coord(alpha, last_point[2], [last_point[0], last_point[1]], dist_to_wheels)]
for i in range(len(encoders)):
    point, icr_f = forward(encoders[i], dist_to_wheels, alpha, beta, enc_const, last_point, t)
    # TODO must return wheels and ICR for drawing robot
    point = point.flatten()
    draw_point(point)

    icr_f_list.append(icr_f)
    trajectory.append(point)
    last_point = point

    wheel_coord = find_wheel_coord(alpha, last_point[2], [last_point[0], last_point[1]], dist_to_wheels)
    wheel_coord_list.append(wheel_coord)
#
# # Draw icr
# for icr_f in icr_f_list:
#     draw_point(icr_f, marker='y^')
#     # plt.pause(1)

# Draw trajectory
for i in range(len(trajectory)):
    if i == 0:
        continue
    draw_line(trajectory[i-1], trajectory[i])
    # plt.pause(1)

# Draw machine
for j in range(len(wheel_coord_list)):
    for k in range(len(wheel_coord_list[j])):
        if k == 0:
            draw_line(wheel_coord_list[j][0], wheel_coord_list[j][len(wheel_coord_list[j]) - 1], 'y--')
        draw_line(wheel_coord_list[j][k], wheel_coord_list[j][k - 1], 'y--')
    # plt.pause(1)

show_drawing()

trajectory = [[trajectory[i][0], trajectory[i][1], trajectory[i][2]] for i in range(len(trajectory))]
print(trajectory)

