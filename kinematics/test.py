import numpy as np
from modules.forward import forward
from modules.inverse import inverse
from modules.draw import *
from modules.find_wheel_param import find_wheel_coord
# import matplotlib
# matplotlib.use('TkAgg')


map = np.ones((10, 10))
map[0, 0] = 0
draw_map(map, blank=True)

start = np.asarray([5, 5, 0])    # 0.7853981633974483
dist_to_wheels = [7.0710678118654752440084436210485, 7.0710678118654752440084436210485]
alpha = [np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4]
beta = np.asarray([- np.pi/4, - 3*np.pi/4])
enc_const = np.asarray([10, 10])
t = 1
last_point = np.asarray(start)
preferable_speed = 0.5     # 6.2
#    .
# O     O
# a = forward(encoders=[np.pi/4, -np.pi/4], dist_to_wheels=[7.0710678118654752440084436210485, 7.0710678118654752440084436210485],
#             alpha=[np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4], beta=[- np.pi/4, - 3*np.pi/4],
#             enc_const=[10, 10],
#             last_point=[10, 5, 0], t=1)
# print(a)

# Test inverse---------------------------------------
trajectory = \
    [[5, 5, 0], [6.876995632153007, 5.018035508547911, -0.011549972991138802],
     [8.754074373333587, 5.014391047273534, -0.023099945982277603],
     [10.63098582049979, 4.989067102349452, -0.034649918973416405],
     [12.507479592926757, 4.942067051999857, -0.046199891964555206],
     [14.383305365607598, 4.8733971660499265, -0.05774986495569401],
     [16.258212902646857, 4.783066605089459, -0.06929983794683281],
     [18.131952090642073, 4.671087419250796, -0.08084981093797161]]

# Draw start
draw_point(start, marker='cx')
# Draw points, trajectory and wheels of generated forward
for i in range(len(trajectory)):
    wheel_coord = find_wheel_coord(alpha,  trajectory[i][2], [trajectory[i][0],  trajectory[i][1]], dist_to_wheels)
    for j in range(len(wheel_coord)):
        if j == 0:
            draw_line(wheel_coord[0], wheel_coord[len(wheel_coord) - 1], 'g--')
        draw_line(wheel_coord[j], wheel_coord[j-1], 'g--')
    if i == 0:
        continue
    draw_line(trajectory[i-1], trajectory[i])
    draw_point(trajectory[i])
    # plt.pause(1)
show_drawing()
plt.close()
plt.cla()
plt.clf()

encoder_list = []
icr_i_list = []
icr_f_list = []
new_trajectory = []
wheel_coord_list = []
for i in range(len(trajectory)):
    if i == 0:
        last_point = trajectory[i]
        continue
    # elif i == 1:
    #     point = trajectory[i]
    #     continue
    else:
        wheel_coord = find_wheel_coord(alpha, last_point[2], [last_point[0], last_point[1]], dist_to_wheels)
        wheel_coord_list.append(wheel_coord)

        next_point = trajectory[i]
        encoders = inverse(preferable_speed, dist_to_wheels, alpha, beta, enc_const, last_point, next_point, t)
        encoder_list.append(encoders)
        # icr_i_list.append(icr)

        point_f, icr_f = forward(encoders, dist_to_wheels, alpha, beta, enc_const, last_point, t)
        point_f = point_f.flatten()
        # draw_point(last_point)

        icr_f_list.append(icr_f)
        new_trajectory.append(point_f)
        last_point = next_point

print(encoder_list)
encoders = encoder_list


draw_map(map, blank=True)
# Draw start
draw_point(start, marker='cx')

# # Draw icr forward (
# for icr_f in icr_f_list:
#     draw_point(icr_f, marker='y^')
#     # plt.pause(1)
#
# # Draw icr inverse
# for icr in icr_i_list:
#     draw_point(icr, marker='g^')
#     # plt.pause(1)

# Draw trajectory
for i in range(len(new_trajectory)):
    if i == 0:
        draw_line(trajectory[i], new_trajectory[i])
        continue
    draw_point(trajectory[i])
    draw_line(trajectory[i], new_trajectory[i])
    # plt.pause(1)

# Draw machine
for j in range(len(wheel_coord_list)):
    for k in range(len(wheel_coord_list[j])):
        if k == 0:
            draw_line(wheel_coord_list[j][0], wheel_coord_list[j][len(wheel_coord_list[j]) - 1], 'y--')
        draw_line(wheel_coord_list[j][k], wheel_coord_list[j][k - 1], 'y--')
    # plt.pause(1)

show_drawing()
plt.close()
plt.cla()
plt.clf()

trajectory = [[new_trajectory[i][0], new_trajectory[i][1], new_trajectory[i][2]] for i in range(len(new_trajectory))]
print(trajectory)


# When on axis of ICR then always works
# O  .  O
# a = forward(encoders=[np.pi/2, 0], dist_to_wheels=[0.5, 0.5],   # + 1e-10
#             alpha=[np.pi / 2, -np.pi / 2], beta=[0., -np.pi],
#             enc_const=[1., 1.],
#             last_point=[5, 5, 0], t=1.)
# print(a)

# O     O
#    .
# a = forward(encoders=[np.pi/2, -np.pi/2], dist_to_wheels=[0.70710678118654752440084436210485, 0.70710678118654752440084436210485],
#             alpha=[np.pi/4, -np.pi/4], beta=[np.pi/4, -5*np.pi/4],
#             enc_const=[1, 1],
#             last_point=[4.5, 5, 0], t=1)
# print(a)

#    .
# O     O
# a = forward(encoders=[np.pi/4, -np.pi/4], dist_to_wheels=[7.0710678118654752440084436210485, 7.0710678118654752440084436210485],
#             alpha=[np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4], beta=[- np.pi/4, - 3*np.pi/4],
#             enc_const=[10, 10],
#             last_point=[10, 5, 0], t=1)
# print(a)

#       .
# O     O
# a = forward(encoders=[np.pi/2, 1], dist_to_wheels=[1.1180339887498948482045868343656, 0.5],
#             alpha=[np.pi/2 + 26.565/180*np.pi, -np.pi], beta=[- 26.565/180*np.pi, np.pi/2],
#             enc_const=[1, 1],
#             last_point=[5.5, 4.5, 0], t=1)
# print(a)

#    O
# O  .
# a = forward(encoders=[2, 1], dist_to_wheels=[0.5, 0.5],
#             alpha=[np.pi / 2, 0], beta=[0, np.pi/2],
#             enc_const=[1, 1],
#             last_point=[5, 5, 0], t=1)
# print(a)

#  <-O
# O  .
# a = forward(encoders=[1., -0.5], dist_to_wheels=[0.5, 0.5],
#             alpha=[np.pi / 2, 0.], beta=[0., 0.],
#             enc_const=[1., 1.],
#             last_point=[5., 5., 0.], t=1.)
# print(a)

#  O
#  .
#  O
# a = forward(encoders=[np.pi/2, np.pi/2], dist_to_wheels=[0.5, 0.5],  # 2.2214
#             alpha=[0., 3.1415926535897], beta=[3.1415926535897/4, -3.1415926535897/2],
#             enc_const=[1., 1.],
#             last_point=[5.0, 5.0, 0], t=1.)
# print(a)
