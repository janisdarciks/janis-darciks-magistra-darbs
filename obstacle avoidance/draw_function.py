import matplotlib.pyplot as plt
import numpy as np


# plt.gca().invert_yaxis()  # inverts map
# plt.grid()
plt.tick_params(labelsize=20)

dist = [i*0.1 for i in range(0, 29, 1)]
CRIT_DIST = 0.6
PUSH_CONST = 0.2
DECAY_CONST = 50
force_list = []
for i in range(len(dist)):
    if np.abs(dist[i]) < CRIT_DIST:#  or np.abs(dist[i]) > -CRIT_DIST:
        force = PUSH_CONST
    # if (dist > 1.5)return 0; // obstacles that are further does not push
    else:
        force = PUSH_CONST / (pow(np.abs(dist[i]) - CRIT_DIST, 2) * DECAY_CONST + 1)
    force_list.append(force)
for i in range(len(dist)):
    if i == 0:
        continue
    # plt.plot(dist[i], force_list[i], 'rx')  # , markersize=8)
    plt.plot([dist[i - 1], dist[i]],
             [force_list[i - 1], force_list[i]], "r-")
plt.xlabel("l, m", size=20)
plt.ylabel("F, N", size=20)
plt.show()
