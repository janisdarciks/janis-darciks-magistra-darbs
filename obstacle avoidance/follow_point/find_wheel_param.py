import numpy as np


def find_wheel_coord(alpha, theta, point, dist_to_wheels):
    point = point[0:2]
    wheel_coord = []
    for i in range(len(alpha)):  # so does not work for 1 wheeled vehicles
        # Rotate to wheel
        rot = np.asarray([[np.cos(alpha[i] + theta), np.sin(alpha[i] + theta)],
                         [-np.sin(alpha[i] + theta), np.cos(alpha[i] + theta)]])
        point_wheel = np.matmul(rot, point)
        # Translate to wheel
        # wheel1 = last_point[0][0] + wheel_speeds[0][0]
        point_wheel[0] = point_wheel[0] + dist_to_wheels[i]
        # Rotate back
        rot = np.asarray([[np.cos(-alpha[i] - theta), np.sin(-alpha[i] - theta)],
                         [-np.sin(-alpha[i] - theta), np.cos(-alpha[i] - theta)]])
        point_wheel = np.matmul(rot, point_wheel)
        wheel_coord.append(point_wheel)
    return wheel_coord


def find_wheel_center(point, dist_to_wheels, alpha):
    # Find wheel coordinates
    wheel_cood = find_wheel_coord(alpha, point[2], [point[0], point[1]], dist_to_wheels)
    # Collect all x in one list and y in other
    x_list = [wheel_cood[i][0] for i in range(len(wheel_cood))]
    y_list = [wheel_cood[i][1] for i in range(len(wheel_cood))]
    # Find mean
    x_mean = np.sum(x_list) / len(wheel_cood)
    y_mean = np.sum(y_list) / len(wheel_cood)

    return [x_mean, y_mean]
