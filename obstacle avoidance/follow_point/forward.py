import numpy as np


# encoders - encoder ticks;
# dist_to_wheel - distance from point to each wheel;
# alpha - point to wheel;
# beta - alpha to wheel vertical;
# enc_const - constant to transform encoder ticks to distance in meters;
# last_point - previous point in global;
# t - time in which encoder ticks where counted.
def forward(encoders, dist_to_wheels, alpha, beta, enc_const, last_point, t):
    last_point = np.asarray(last_point)     # (3,) x, y, theta
    encoders = np.asarray(encoders)         # (2,) for each wheel
    enc_const = np.asarray(enc_const)       # (2,) for each wheel (all the same)
    last_point = last_point[np.newaxis].T   # (3, 1)
    encoders = encoders[np.newaxis].T       # (2, 1)
    theta = last_point[2][0]                # robot direction in global
    rot_matrix_inv = np.asarray([[np.cos(theta), -np.sin(theta), 0.],
                                 [np.sin(theta), np.cos(theta), 0.],
                                 [0., 0., 1.]])

    # Builds matrices for specific wheel count
    size = enc_const.shape[0]               # = wheel count = 2
    j1 = np.zeros((size, 3))                # (2, 3)
    enc_const_matrix = np.zeros((size*2, size*2))   # (4, 4) first half will be diagonal matrix
    c1 = np.zeros((size, 3))                # (2, 3)
    for i in range(size):
        j1[i] = [np.sin(alpha[i] + beta[i]), -np.cos(alpha[i] + beta[i]), -dist_to_wheels[i] * np.cos(beta[i])]
        c1[i] = [np.cos(alpha[i] + beta[i]), np.sin(alpha[i] + beta[i]), dist_to_wheels[i] * np.sin(beta[i])]
        enc_const_list = []
        for j in range(size*2):
            if j == i:
                enc_const_list.append(enc_const[i])
            else:
                enc_const_list.append(0)
        enc_const_matrix[i] = enc_const_list
    j1 = np.concatenate((j1, c1))           # (4, 3)
    j1_inv = np.linalg.pinv(j1)             # (3, 4)

    # To calculate each coordinate velocity --> || J1 * rot_matrix_inv * xi - J2 * phi = 0 ||
    encoders = np.concatenate((encoders, np.zeros((size, 1))))                                      # (4, 1)
    xi = np.matmul(rot_matrix_inv, np.matmul(j1_inv, np.matmul(enc_const_matrix / t, encoders)))    # (3, 1) x, y, theta velocities
    last_point_speed = np.sqrt(np.power(xi[0][0], 2) + np.power(xi[1][0], 2))
    omega = xi[2][0]    # angular velocity
    if -1e-8 < omega < 1e-8:   # to not divide by 0
        omega = 1e-6
    delta_theta = omega * t  # angle that is made by moving from last_point to new_point
    icr_radius = last_point_speed / omega  # radius made by moving from last_point to new_point
    velocity_angle = np.arctan2(xi[1][0], xi[0][0])

    # Finds ICR - instantaneous center of rotation
    icr = np.asarray([last_point[0][0] - icr_radius * np.sin(velocity_angle),   # (3, 1)
                      last_point[1][0] + icr_radius * np.cos(velocity_angle),
                      delta_theta])[np.newaxis].T

    # Translate from global to ICR
    point_icr = np.subtract(last_point, icr)                                    # (3, 1)
    point_icr[2][0] = last_point[2][0]

    # Point rotation around ICR and translation to global
    rot_matrix_icr = np.asarray([[np.cos(delta_theta), -np.sin(delta_theta), 0.],
                                 [np.sin(delta_theta), np.cos(delta_theta), 0.],
                                 [0., 0., 1.]])
    new_point = np.add(np.matmul(rot_matrix_icr, point_icr), icr)               # (3, 1)
    new_point[2][0] = (new_point[2][0] + np.pi) % (2 * np.pi) - np.pi  # angle normalization

    return new_point, last_point_speed
