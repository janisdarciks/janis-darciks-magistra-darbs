import numpy as np
from follow_point.find_wheel_param import find_wheel_coord


# speed - desirable speed to move point;
# dist_to_wheel - distance from point to each wheel;
# alpha - point to wheel;
# beta - alpha to wheel vertical;
# enc_const - constant to transform encoder ticks to distance in meters;
# point_from - previous point in global;
# point_to - global coordinates where to move;
# pid_angle - for regulating angle,
# t - time in which encoder ticks where counted (= 1 iter).

# If wheel_coord and j1 are given in function then dist_to_wheels, enc_const not needed
# alpha and beta only needed for finding icr (first wheels angle)
def inverse(dist_to_wheels, alpha, beta, enc_const, point_from, point_to, t, speed=5):
    enc_const = np.asarray(enc_const)                       # (2,)
    theta = point_from[2]    # robot direction in global
    size = enc_const.shape[0]   # matches to wheel count, for building j1, c1 matrices

    diff_x = point_to[0] - point_from[0]
    diff_y = point_to[1] - point_from[1]
    velocity_angle = np.arctan2(diff_y, diff_x)

    # Finds delta theta (angle difference from last_point to point)
    if np.sign(theta) == np.sign(velocity_angle):
        delta_theta = velocity_angle - theta
    else:
        if np.abs(velocity_angle) < np.pi/2:
            delta_theta = velocity_angle - theta
        elif np.abs(velocity_angle + 2*np.pi) - np.abs(theta) < \
            np.abs(velocity_angle - 2*np.pi) - np.abs(theta):
            # next_velocity_angle was negative and velocity_angle is positive
            delta_theta = velocity_angle + 2 * np.pi - theta
        else:
            # next_velocity_angle was positive and velocity_angle is negative
            delta_theta = velocity_angle - 2 * np.pi - theta

    # Finds coordinates of wheels in global (can be given in function)
    wheel_coord = find_wheel_coord(alpha, theta, point_from, dist_to_wheels)    # (list: 2)

    # To find ICR - instantaneous center of rotation two line cross point must be found
    # Line from 1. wheel
    if -1e-8 < theta < 1e-8:
        theta = 1e-7
    angle1 = theta + alpha[0] + beta[0]              # float
    angle1 = (angle1 + np.pi) % (2 * np.pi) - np.pi  # angle normalization
    a = np.tan(angle1)                               # float
    b = - a * wheel_coord[0][0] + wheel_coord[0][1]  # float
    # Line from velocity direction
    c = np.tan(velocity_angle + np.pi/2)             # float
    d = - c * point_from[0] + point_from[1]          # float
    icr_x = (d - b) / (a - c)                        # float
    if np.abs(a) > 1e4 and np.abs(b) > 1e4 and np.abs(c) > 1e4 and np.abs(d) > 1e4:
        icr_y = 1e7
    else:
        icr_y = (c * icr_x + d)                      # float
    icr_radius = np.sqrt(np.power(icr_x - point_from[0], 2) + np.power(icr_y - point_from[1], 2)) + 1e-7    # float

    # Find angular speed - omega and its direction
    # if delta_theta >= 0:
    #     omega = 1 / icr_radius               # float
    # else:
    #     omega = -1 / icr_radius
    #
    # xi = np.array([np.cos(velocity_angle), np.sin(velocity_angle), omega])[np.newaxis].T    # (3, 1)
    if delta_theta >= 0:
        omega = speed / icr_radius               # float
    else:
        omega = -speed / icr_radius

    xi = np.array([np.cos(velocity_angle)*speed, np.sin(velocity_angle)*speed, omega])[np.newaxis].T    # (3, 1)
    rot_matrix = np.asarray([[np.cos(theta), np.sin(theta), 0.],    # (3, 3)
                             [-np.sin(theta), np.cos(theta), 0.],
                             [0., 0., 1.]])

    # Builds matrices for specific wheel count
    # Same as in forward (so can be given in function)
    j1 = np.zeros((size, 3))                                            # (2, 3)
    enc_const_matrix = np.zeros((size*2, size*2))   # diagonal matrix     (4, 4)
    c1 = np.zeros((size, 3))                                            # (2, 3)
    for i in range(size):
        j1[i] = [np.sin(alpha[i] + beta[i]), -np.cos(alpha[i] + beta[i]), -dist_to_wheels[i] * np.cos(beta[i])]
        c1[i] = [np.cos(alpha[i] + beta[i]), np.sin(alpha[i] + beta[i]), dist_to_wheels[i] * np.sin(beta[i])]
        enc_const_list = []
        for j in range(size*2):
            if j == i:
                enc_const_list.append(enc_const[i])
            else:
                enc_const_list.append(0)
        enc_const_matrix[i] = enc_const_list
    j1 = np.concatenate((j1, c1))                                       # (4, 3)
    # No j1_inv

    # To calculate each wheel encoder value --> || J1 * rot_matrix_inv * xi - J2 * phi = 0 ||
    enc_const_matrix_inv = np.linalg.pinv(enc_const_matrix/t)           # (4, 4)
    encoders = np.matmul(enc_const_matrix_inv, np.matmul(j1, np.matmul(rot_matrix, xi)))    # (4, 1)

    encoders = encoders[:len(encoders) - size]
    encoders = [encoders[i][0] for i in range(len(encoders))]           # (list: 2)

    return encoders

#

# x = [-2.5, 0.0, 2.5]    # , 5.0, 7.5, 3.0, -1.0]
# y = [0.7, -6, 5]    # , 6.5, 0.0, 5.0, -2.0]

# O  .  O
# a = inverse(speed=np.pi/4., point=[0.0, -6], dist_to_wheels=[0.5, 0.5],
#             alpha=[np.pi / 2, -np.pi / 2], beta=[0., np.pi],
#             enc_const=[1., 1.],
#             last_point=[-2.5, 0.7, 0], next_point=[2.5, 5], t=1)
# print(a)

# O  .  O
# a = inverse(speed=np.pi/4., point=[5.5, 4.5], dist_to_wheels=[0.5, 0.5],
#             alpha=[np.pi / 2, -np.pi / 2], beta=[0., np.pi],
#             enc_const=[1., 1.],
#             last_point=[5., 5., 0], next_point=[5, 4], t=1)
# print(a)


#    .
# O     O
# a = inverse(speed=0.3927, point=[0.0, -6], dist_to_wheels=[0.70710678118654752440084436210485, 0.70710678118654752440084436210485],
#             alpha=[np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4], beta=[- np.pi/4, - 3*np.pi/4],
#             enc_const=[1, 1],
#             last_point=[-2.5, 0.7, 0], next_point=[2.5, 5], t=1)
# print(a)


#  O
#  .
#  O
# a = inverse(speed=2.0523429487018494, point=[6.30656296, 3.69343839, -1.57079529], dist_to_wheels=[0.5, 0.5],
#             alpha=[0., 3.1415926535897], beta=[3.1415926535897/4, -3.1415926535897/2],
#             enc_const=[1., 1.],
#             last_point=[5.0, 5.0, 0], t=1.)
# print(a)


#       .
# O     O
# a = inverse(speed=1.6285711562775937, point=[4.36909593, 4.94615241, -2.57079518 + 1.570796], dist_to_wheels=[1.1180339887498948482045868343656, 0.5],
#             alpha=[np.pi/2 + 26.565/180*np.pi, -np.pi], beta=[- 26.565/180*np.pi, np.pi/2],
#             enc_const=[1, 1],
#             last_point=[5.5, 4.5, np.pi/2], t=1)
# print(a)
