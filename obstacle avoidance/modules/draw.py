import matplotlib.pyplot as plt
import matplotlib.animation as animation


def draw_map(map, blank=False):
    if blank:
        plt.close()
    fig, ax = plt.subplots()
    ax.imshow(map, cmap='gray')  # shows map
    plt.gca().invert_yaxis()  # inverts map
    plt.grid()
    # plt.tick_params(labelsize=20)


def draw_point(point, marker='rx'):
    plt.plot(point[0], point[1], marker)    # , markersize=8)


def draw_line(point1, point2, line='b-'):
    plt.plot([point1[0], point2[0]],
             [point1[1], point2[1]], line)  # , linewidth=6.0)


def draw_trajectory(point_list, line='b-'):
    for i in range(len(point_list)):
        if i == 0:
            continue
        plt.plot([point_list[i-1][0], point_list[i][0]],
                 [point_list[i-1][1], point_list[i][1]], line)   # , linewidth=3.0)


def show_drawing():
    plt.show()


def show_animation(func, func_param, map):
    fig1 = plt.figure()
    # data = np.random.rand(2, 25)
    # l, = plt.plot([], [], 'r-')
    plt.xlim(0, len(map[0]))
    plt.ylim(0, len(map))
    # plt.xlabel('x')
    # plt.title('test')
    line_ani = animation.FuncAnimation(fig1, func, 25, interval=50, fargs=func_param, blit=True)
