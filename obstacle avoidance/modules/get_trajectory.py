import numpy as np
# import copy
from follow_point.cubic_spline_planner import Spline2D
from follow_point.inverse import inverse
from follow_point.forward import forward


def get_trajectory(start, trajectory, rob_pram, spline_step, DIST_POINT_TO, DIST_TO_GOAL):
    dist_to_wheels = rob_pram['dist_to_wheels']
    alpha = rob_pram['alpha']
    beta = rob_pram['beta']
    enc_const = rob_pram['enc_const']
    t = rob_pram['t']

    # Trajectory generation
    point_listx = [start[0]]
    point_listy = [start[1]]
    for i in range(len(trajectory)):
        point_listx.append(trajectory[i][0])
        point_listy.append(trajectory[i][1])
    ending = trajectory[-1]

    # Generate smooth path using spline
    sp = Spline2D(point_listx, point_listy)
    s = np.arange(0, sp.s[-1], spline_step)     # divide trajectory in smooth path for testing
    trajectory = []
    tmp_list = []
    for i_s in s:
        ix, iy = sp.calc_position(i_s)
        tmp_list.append(ix)
        tmp_list.append(iy)
        trajectory.append(tmp_list)
        tmp_list = []
    trajectory.append(ending)

    point_listx = []
    point_listy = []
    for i in range(len(trajectory)):
        point_listx.append(trajectory[i][0])
        point_listy.append(trajectory[i][1])

    encoder_list = []
    new_trajectory = []
    point_from = start
    # copy_point_from = copy.deepcopy(point_from)
    point_to = trajectory.pop(0)
    while True:
        # Find encoder values to aim to point
        encoders = inverse(dist_to_wheels, alpha, beta, enc_const, point_from, point_to, t)
        encoder_list.append(encoders)

        # Find where vehicle would go with found encoder values
        point_f, velocity_f = forward(encoders, dist_to_wheels, alpha, beta, enc_const, point_from, t)
        point_f = point_f.flatten()

        # Check dist to point
        # TODO needs while loop for finding closest point on trajectory and drop ones
        dist = np.sqrt(np.power(point_to[0] - point_f[0], 2) + np.power(point_to[1] - point_f[1], 2))
        if dist < DIST_POINT_TO:
            if trajectory:  # if list not empty (last point)
                # Case 1: change last point when when point to ie reached
                # last_point = copy_point_from
                # copy_point_from = copy.deepcopy(point_from)
                point_to = trajectory.pop(0)

            # Finishing
            else:
                if dist < DIST_TO_GOAL:     # Finished
                    new_trajectory.append(point_f)
                    break
                else:   # Some last iterations
                    pass
        # else:
        #     while True:
        #         dist2 = np.sqrt(np.power(next_point[0] - point_f[0], 2) + np.power(point_to[1] - point_f[1], 2))
        #         if dist < dist2 :    # if point_to is closer than next_point to point_f   or dist > 0.3
        #             break
        #         else:               # skip point to and choose next
        #             if trajectory:
        #                 # Case 1
        #                 # last_point = copy_point_from
        #                 # copy_point_from = copy.deepcopy(point_from)
        #                 dist = dist2
        #                 point_to = next_point
        #                 next_point = trajectory.pop(0)
        #             else:
        #                 break

        new_trajectory.append(point_f)
        # Case 2: change last point each iter
        # last_point = point_from
        point_from = point_f
        # Case 3: (in inverse diff_x, diff_y changes )

    return new_trajectory
