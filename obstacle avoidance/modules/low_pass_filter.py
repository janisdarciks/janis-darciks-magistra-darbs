import numpy as np
from scipy.signal import butter, lfilter, lfilter_zi


def butter_lowpass(cutoff, fs, order=2):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(path, coordinate, cutoff=1, fs=2.4, order=2):     # 2.3 or 2.4 best fs
    if not (coordinate == 0 or coordinate == 1):
        print("coordinate: 0 = x, 1 = y. No other values expected")

    # Separate data
    tmp_path = []
    for i in path:
        tmp_path.append(i[coordinate])
    measurement = np.asarray(tmp_path)

    # Filter
    b, a = butter_lowpass(cutoff, fs, order=order)
    zi = lfilter_zi(b, a)
    predictions, zf = lfilter(b, a, measurement, zi=zi*measurement[0])

    # Concatenate data back
    predictions = list(predictions)
    for i in range(len(path)):
        predictions[i] = float(predictions[i])
        path[i][coordinate] = predictions[i]
    return path


# # Filter requirements.
# order = 6
# fs = 30.0       # sample rate, Hz
# cutoff = 3.667  # desired cutoff frequency of the filter, Hz

# T = 5.0
# n = T * fs
# t = np.linspace(0, T, n, endpoint=False)
# data = np.sin(1.2*2*np.pi*t) + 1.5*np.cos(9*2*np.pi*t) + 0.5*np.sin(12.0*2*np.pi*t)
#
# # Filter the data, and plot both the original and filtered signals.
# y = butter_lowpass_filter(data, cutoff, fs, order)


# plt.plot(t, data, 'b-', label='data')
# plt.plot(t, y, 'g-', linewidth=2, label='filtered data')
# plt.xlabel('Time [sec]')
# plt.grid()
# plt.legend()
#
# plt.subplots_adjust(hspace=0.35)
# plt.show()
# a = 1