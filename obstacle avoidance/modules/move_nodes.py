import numpy as np
import copy
from modules.kalman_filter import kalman_filter
from modules.low_pass_filter import butter_lowpass_filter


# point - x, y, angle list or array;
# trajectory - list of all points;
# map - whole global map;
# min_max_values - values to define local box (min_y, max_y, min_x, max_x);
# push_const1 - constant that regulates force (vector length) to push next_point away from obstacles;
# push_const2 - constant that regulates force (vector length) to push next_point away from point;
# max_push_dist - maximal distance to push next_point;
# pull_const - constant that regulates force (vector length) to pull some next points towards next_point
# max_pull_dist - maximal distance to pull points (to avoid anomalies)
# max_point_dist - distance that is compared to distance between pulled nodes for new point generation
# pull_point_count - constant that shows how much points after next_point to pull toward

def move_nodes(point, trajectory, map, min_max_values, push_const1=.4, push_const2=30, max_push_dist=20,
               pull_const=.01, max_pull_dist=20, max_point_dist=18, pull_point_count=3):
    # Select area in map
    area = map[min_max_values[0]: min_max_values[1], min_max_values[2]: min_max_values[3]]  # (60, 60)

    # Firstly checks if there is any pixel at all
    if np.all(area):  # if no obstacles continue
        return trajectory

    # Find obstacle pixels that will move trajectory nodes
    obstacle_list = []
    for m, pixel_line in enumerate(area):
        for n, pixel in enumerate(pixel_line):
            if not pixel:       # if pixel is taken
                obstacle_list.append([n + min_max_values[2], m + min_max_values[0]])

    # Push next_point away from obstacles
    next_point = trajectory[0]       # only first node in trajectory pushed by obstacles  (x, y, angle list or array)
    force_from_obst_linear = [0, 0]
    force_from_obst = 0
    for j in range(len(obstacle_list)):
        # Calculates distance from pixel to node (obstacle in node coordinates)
        dx = obstacle_list[j][0] - next_point[0]
        dy = obstacle_list[j][1] - next_point[1]
        dist_to_obst = np.hypot(dx, dy)
        if dist_to_obst <= 1:   # not to skyrocket
            continue
        # The closer is obstacle the greater is force_from_obst component
        force_from_obst_linear[0] += dx     # for angle
        force_from_obst_linear[1] += dy     # for angle
        force_from_obst += -push_const1 / dist_to_obst

    angle_from_obst = np.arctan2(force_from_obst_linear[1], force_from_obst_linear[0])
    next_point_from_obst = [0, 0]
    next_point_from_obst[0] = force_from_obst * np.cos(angle_from_obst)
    next_point_from_obst[1] = force_from_obst * np.sin(angle_from_obst)

    # Pull closer to point
    dx = next_point[0] - point[0]
    dy = next_point[1] - point[1]
    dist_to_point = np.hypot(dx, dy)
    next_point_from_point = [0, 0]
    force_from_point = -push_const2 / dist_to_point
    angle_from_point = np.arctan2(dy, dx)
    next_point_from_point[0] = force_from_point * np.cos(angle_from_point)
    next_point_from_point[1] = force_from_point * np.sin(angle_from_point)

    # Total force from pushing away from obstacles and pushing away from point
    next_point_dist_x = next_point_from_point[0] + next_point_from_obst[0]
    next_point_dist_y = next_point_from_point[1] + next_point_from_obst[1]

    # To regulate maximal push distance
    # next_point_dist = np.hypot(next_point_dist_x, next_point_dist_y)
    # if next_point_dist > max_push_dist:
    #     angle_to_push = np.arctan2(next_point_dist_y, next_point_dist_x)
    #     next_point_dist_x = max_push_dist * np.cos(angle_to_push)
    #     next_point_dist_y = max_push_dist * np.sin(angle_to_push)

    # New next point coordinates
    next_point[0] += next_point_dist_x
    next_point[1] += next_point_dist_y

    # Change trajectory
    trajectory[0] = next_point

    # # ____________________________________________________
    # # Further nodes in trajectory pulled towards next_point (points are pulled to last regulated one)
    # pulled_point_list = copy.deepcopy(trajectory[1:1 + pull_point_count])
    # pulled_point_list.insert(0, next_point)
    #
    # for i, tmp_point in enumerate(pulled_point_list):
    #     if i == 0:      # no use to pull away from your self
    #         continue
    #     dx = next_point[0] - tmp_point[0]
    #     dy = next_point[1] - tmp_point[1]
    #     angle_pull = np.arctan2(dy, dx)             # angle where to pull point
    #     dist_to_next_point = np.hypot(dy, dx)       # dist to pull
    #
    #     r2 = dist_to_next_point**2 * pull_const     # the further away from each other the greater attraction
    #
    #     # Make limit how close to pull point maximally
    #     if r2 > max_pull_dist:
    #         r2 = max_pull_dist
    #     if r2 < -max_pull_dist:
    #         r2 = -max_pull_dist
    #
    #     # Pull each component
    #     dx = np.cos(angle_pull) * r2
    #     dy = np.sin(angle_pull) * r2
    #     pulled_point_list[i][0] = dx + tmp_point[0]
    #     pulled_point_list[i][1] = dy + tmp_point[1]
    #
    #     next_point = tmp_point      # pull next one after last pulled node
    #
    # # pulled_point_list = butter_lowpass_filter(pulled_point_list, 0)
    # # pulled_point_list = butter_lowpass_filter(pulled_point_list, 1)
    #
    # trajectory[:1 + pull_point_count] = pulled_point_list
    #
    # # _____________________________________________________
    # # Generate more points between two first pulled points
    # counter = 0
    # while True:     # while there is space greater than max_point_dist
    #     tmp_point1 = copy.deepcopy(trajectory[counter+1])
    #     tmp_point2 = copy.deepcopy(trajectory[counter+2])
    #     dist = np.hypot(tmp_point1[0] - tmp_point2[0], tmp_point1[1] - tmp_point2[1])
    #     if dist >= max_point_dist:
    #         angle = np.arctan2(tmp_point2[1] - tmp_point1[1], tmp_point2[0] - tmp_point1[0])
    #         tmp_insertion_point = [0, 0, angle]
    #         tmp_insertion_point[0] = max_point_dist * np.cos(angle) + tmp_point1[0]
    #         tmp_insertion_point[1] = max_point_dist * np.sin(angle) + tmp_point1[1]
    #         trajectory.insert(counter+2, tmp_insertion_point)
    #     else:
    #         break
    #     counter += 1
    #
    # # _____________________________________________________
    # # Filter all newly transformed path
    # point_list = trajectory[:1+pull_point_count+counter]    # all newly moved points + generated ones between
    # point_list = np.insert(np.array(point_list), [0], np.array(point), axis=0)  # also point for smoothness
    # point_list = butter_lowpass_filter(point_list, 0)   # filter first coordinate, x
    # point_list = butter_lowpass_filter(point_list, 1)   # filter second coordinate, y
    # point_list = np.delete(point_list, 0, 0)            # pop point
    #
    # trajectory[:1+pull_point_count+counter] = point_list


    return trajectory

