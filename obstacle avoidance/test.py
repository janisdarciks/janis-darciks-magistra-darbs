import numpy as np
import copy
from modules.draw import *
from modules.get_trajectory import get_trajectory
from modules.move_nodes import move_nodes
from modules.kalman_filter import KalmanFilter, kalman_filter
from modules.low_pass_filter import butter_lowpass_filter


animation = True

if animation:
    import matplotlib
    matplotlib.use('QT4Agg')

# 1 - free pixel, 0 - taken
map = np.ones((500, 600))
# map[0, 0] = 0

# Obstacles
# map[18, 5] = 0
# map[280:320, 200:240] = 0
map[340:450, 360:380] = 0
# map[650:700, 390:450] = 0
# map[170:210, 300:320] = 0


# draw_map(map, blank=True)

# Vehicle parameters
start = np.asarray([150, 150, np.pi/2])
rob_parameters = {
    'dist_to_wheels': [0.70710678118654752440084436210485, 0.70710678118654752440084436210485],
    'alpha': [np.pi / 2 + np.pi/4, -np.pi / 2 - np.pi/4],
    'beta': [- np.pi/4, - 3*np.pi/4],
    'enc_const': [1e-6, 1e-6],
    't': 0.05,
    'rob_width': 16
}
DIST_TO_GOAL = 0.5
DIST_POINT_TO = 2
line_step = 6

# Trajectory generation
trajectory = [[150, 200], [450, 350], [450, 50]]   # , [750, 450], [750, 300], [350, 150]]
tmp_new_trajectory = get_trajectory(start, trajectory, rob_parameters, line_step, DIST_POINT_TO, DIST_TO_GOAL)
# Shorten trajectory
p1 = tmp_new_trajectory.pop(0)
p2 = tmp_new_trajectory.pop(0)
new_trajectory = [p1]
while True:
    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]
    dist = np.sqrt(np.power(dx, 2) + np.power(dy, 2))
    if dist >= line_step:
        new_trajectory.append(p2)
        p1 = p2
    if not tmp_new_trajectory:
        break
    p2 = tmp_new_trajectory.pop(0)


# _________________________________________________________
# Obstacle avoidance
# Size to simulate sensor that checks obstacles ahead
length = 60     # half to each side from point
height = 60      # forward from point

# Find area to check for obstacles
new_new_trajectory = []
copy_new_trajectory = copy.deepcopy(new_trajectory)
last_theta = new_trajectory[0][2]

# draw_map(map, blank=True)
# draw_trajectory(copy_new_trajectory)    # path to go
# plt.show()

# For animation drawing
if animation:
    plt.ion()
    draw_map(map, blank=True)
    draw_trajectory(copy_new_trajectory)    # path to go

last_point = start
count = 0
while True:
    # Builds box. Straight situation (x global and x robot overlap)
    y_min = -length
    y_max = length
    x_min = -height
    x_max = height
    p1 = np.array([x_min, y_min])[np.newaxis].T
    p2 = np.array([x_min, y_max])[np.newaxis].T
    p3 = np.array([x_max, y_max])[np.newaxis].T
    p4 = np.array([x_max, y_min])[np.newaxis].T

    point = new_trajectory.pop(0)
    new_new_trajectory.append(point)
    if new_trajectory:
        theta = np.arctan2(point[1] - last_point[1], point[0] - last_point[0])
    else:
        theta = point[-1]
    point = np.array(point[0:2])[np.newaxis].T
    # Rotate box
    # rotation_matrix = np.asarray([[np.cos(last_theta), -np.sin(last_theta)],
    #                               [np.sin(last_theta), np.cos(last_theta)]])
    rotation_matrix = np.asarray([[np.cos(theta), -np.sin(theta)],
                                  [np.sin(theta), np.cos(theta)]])
    p1 = np.matmul(rotation_matrix, p1)
    p2 = np.matmul(rotation_matrix, p2)
    p3 = np.matmul(rotation_matrix, p3)
    p4 = np.matmul(rotation_matrix, p4)

    # Translate to point
    p1 = p1 + point
    p2 = p2 + point
    p3 = p3 + point
    p4 = p4 + point

    # Find new min, max values for new box
    x_min = int(min(p1[0][0], p2[0][0], p3[0][0], p4[0][0]))
    x_max = int(max(p1[0][0], p2[0][0], p3[0][0], p4[0][0]))
    y_min = int(min(p1[1][0], p2[1][0], p3[1][0], p4[1][0]))
    y_max = int(max(p1[1][0], p2[1][0], p3[1][0], p4[1][0]))

    min_max_point_list = [y_min, y_max, x_min, x_max]
    point = [point[0][0], point[1][0], theta]
    # last_theta = theta
    # new_point = move_nodes(point, new_trajectory, map, min_max_point_list, rob_parameters['rob_width'])
    new_trajectory = move_nodes(point, new_trajectory, map, min_max_point_list)

    # For animation drawing
    if animation:
        # draw_point(point)   # where robot is right now
        draw_line(point, new_trajectory[0], 'g-')  # trajectory where it goes next
        # if count == 93:     # 0, 11, 40, 58, 78, 93, 115

        draw_line([x_min, y_min], [x_min, y_max], 'y--')
        draw_line([x_min, y_max], [x_max, y_max], 'y--')
        draw_line([x_max, y_max], [x_max, y_min], 'y--')
        draw_line([x_max, y_min], [x_min, y_min], 'y--')

        if new_trajectory:
            if len(new_trajectory) > 1:
                draw_point(new_trajectory[0], 'c^')     # next_point pushed by obstacles
                draw_point(new_trajectory[1], 'm^')     # next_next_point first of pulled by previous points
                draw_point(new_trajectory[2], 'y^')
                draw_point(new_trajectory[3], 'y*')
                # plt.pause(20)

        plt.pause(.2)
        # plt.pause(10)


    count += 1
    if not new_trajectory:
        break

    # if new_point != point and len(new_point) > 1:
    #     new_trajectory[0:len(new_point)] = new_point
    # elif new_point != point and len(new_point) == 1:
    #     new_trajectory[0] = new_point[0]

    # draw_map(map, blank=True)
    # draw_trajectory(new_trajectory)
    # plt.draw()
    # plt.pause(0.1)
    # plt.show()

if animation:
    plt.show()    # shows point coord and boxes around

draw_map(map, blank=True)
draw_trajectory(new_new_trajectory)     # shifted trajectory
plt.show()

# new_trajectory1 = kalman_filter(new_new_trajectory, 0)
# new_trajectory1 = kalman_filter(new_new_trajectory, 1)

# new_trajectory1 = butter_lowpass_filter(new_new_trajectory, 0)
# new_trajectory1 = butter_lowpass_filter(new_new_trajectory, 1)

# draw_map(map, blank=True)
# draw_trajectory(new_trajectory1)     # shifted trajectory
# plt.show()

# path = new_new_trajectory
# path = kalman_filter(path, 0)
# path = kalman_filter(path, 1)
# path = [[path[i][0], path[i][1]] for i in range(len(path))]
#
# # Display the image
# draw_map(map)
# plt.plot([x for (x, y) in path], [y for (x, y) in path], '--r')
# plt.show()
