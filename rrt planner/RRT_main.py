import matplotlib.pyplot as plt
from modules.get_data import get_data
from modules.get_data_list import get_data_list
from modules.rrt import RRT


SHOW_ANIMATION = True

# Read data from planner
map_data = get_data("map1.jpeg")
map_data['data'] = get_data_list(map_data['data'])


def main():
    print("start " + __file__)

    rrt = RRT(start=[150, 200], goal=[530, 450], map_data=map_data)
    path = rrt.planning(animation=SHOW_ANIMATION)
    print("Path file is made!")

    # Draw final path with all generated connections
    if SHOW_ANIMATION:
        fig, ax = plt.subplots(1)
        ax.imshow(rrt.data_list, cmap='gray')
        rrt.drawing()
        plt.plot([x for (x, y) in path], [y for (x, y) in path], '--r')
        plt.grid(True)
        plt.show()


if __name__ == '__main__':
    main()
