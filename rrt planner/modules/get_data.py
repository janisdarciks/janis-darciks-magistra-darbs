from PIL import Image


# To get data from database
def get_data(filename):
    img = Image.open(filename).convert('L')

    img = img.crop((600, 550, 1400, 1150))      # no cropping for img2

    # Makes image black(1) and white(255) and gets list with data
    x_list = []
    y_list = []
    data = []
    for y in range(img.size[1]):
        for x in range(img.size[0]):
            x_list.append(x)
            y_list.append(y)
    value = list(img.getdata())
    data_list = list(zip(x_list, y_list, value))

    for sample in data_list:
        data.append(sample[0])
        data.append(sample[1])
        data.append(sample[2])

    map_data = {
        'size': img.size,
        'data': data
    }

    return map_data
