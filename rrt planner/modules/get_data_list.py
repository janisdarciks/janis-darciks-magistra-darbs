def get_data_list(data):
    data_line = []
    data_list = []
    for i in range(0, len(data), 3):
        if data[i + 2] >= 230:
            value = True
        else:
            value = False

        if i == 0:
            data_line.append(value)
            continue
        if data[i] < data[i - 3]:
            data_list.append(data_line)
            data_line = []

        data_line.append(value)

    return data_list
