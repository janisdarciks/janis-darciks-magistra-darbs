import numpy as np
import matplotlib.pyplot as plt
import random
import copy
import math
from modules.NodeRRT import Node
from modules.KalmanFilter import KalmanFilter


class RRT():
    def __init__(self, start, goal, map_data,
                 expand_dis=15.0, velocity_to_goal=70, rob_size=20, shorter_path_coef=8):

        self.start = Node(start[0], start[1])
        self.goal = Node(goal[0], goal[1])
        self.data_list = np.array(map_data['data'])
        self.area_size = [0, map_data['size'][0] - 1, 0, map_data['size'][1] - 1]
        self.expand_dis = expand_dis
        self.velocity_to_goal = velocity_to_goal
        self.rob_size = rob_size
        self.shorter_path_coef = shorter_path_coef
        self.node_list = [self.start]

    def planning(self, animation=True):
        # Display the map
        fig, ax = plt.subplots(1)
        ax.imshow(self.data_list, cmap='gray')

        while True:
            # Random Sampling
            if random.randint(0, 100) > self.velocity_to_goal:
                rnd = [random.randint(self.area_size[0], self.area_size[1]),
                       random.randint(self.area_size[2], self.area_size[3])]
            else:
                rnd = [self.goal.x, self.goal.y]

            # Find nearest node
            nind = self.get_nearest_node_idx(self.node_list, rnd)

            # Expand tree (1)
            nearest_node = self.node_list[nind]
            theta = math.atan2(rnd[1] - nearest_node.y, rnd[0] - nearest_node.x)
            dx = nearest_node.x - rnd[0]
            dy = nearest_node.y - rnd[1]
            dist = math.sqrt(dx ** 2 + dy ** 2)

            # Do not add shorter connections
            if dist < self.expand_dis/2:
                continue

            # Expand tree (2)
            new_node = copy.deepcopy(nearest_node)
            new_node.x += self.expand_dis * math.cos(theta)
            new_node.y += self.expand_dis * math.sin(theta)
            new_node.parent = nind

            # Collision detection
            if not self.collision_detection_with_distance(p_one=[nearest_node.x, nearest_node.y],
                                                          p_two=[new_node.x, new_node.y],
                                                          search_radius=self.rob_size):
                continue    # if not safe skip it

            # If no collision add node to list
            self.node_list.append(new_node)

            # Check goal
            dx = new_node.x - self.goal.x
            dy = new_node.y - self.goal.y
            d = math.sqrt(dx * dx + dy * dy)

            if d <= self.expand_dis:
                print("nNodelist:", len(self.node_list))
                print("Goal!!")
                break

        # Draws generated path
        last_index = len(self.node_list) - 1
        path = self.gen_final_course(last_index)

        # Finally draws smoothed path
        for i in range(self.shorter_path_coef):
            path = self.get_shorter_path(path)
            path = path[::-1]
            path = self.node_addition(path, min_dis=1, max_dis=self.rob_size)

        plt.close()
        fig, ax = plt.subplots(1)
        ax.imshow(self.data_list, cmap='gray')
        plt.gca().invert_yaxis()                     # inverts map

        plt.plot([x for (x, y) in path], [y for (x, y) in path], '-b')
        # for i in range(len(path)):
        #     plt.plot(path[i][0], path[i][1], 'oy', markersize=3)

        print(f'len_path {len(path)}')
        # path = self.node_moved_by_obstacle(path)
        path = self.kalman_filter(path, coordinate=0)   # for x axis
        path = self.kalman_filter(path, coordinate=1)   # for y axis
        path.pop(0)

        # Shows final path without firstly generated random tree
        # plt.close()
        # fig, ax = plt.subplots(1)
        # ax.imshow(self.data_intensity, cmap='gray')
        plt.plot([x for (x, y) in path], [y for (x, y) in path], '--r')
        # plt.grid(True)
        plt.pause(1.0)
        plt.show()

        return path

    def kalman_filter(self, path, coordinate):  # coordinate: 0 = x, 1 = y
        if not (coordinate == 0 or coordinate == 1):
            print("coordinate: 0 = x, 1 = y. No other values expected")

        dt = 1.0 / 60
        F = np.array([[1, dt, 0], [0, 1, dt], [0, 0, 1]])
        H = np.array([1, 0, 0]).reshape(1, 3)
        Q = np.array([[0.01, 0.01, 0.01], [-0.01, 0.01, 0.001], [-0.01, -0.01, -0.01]])
        R = np.array([0.2]).reshape(1, 1)

        # Chooses the right beginning node even if the path is flipped and chosen coordinate
        if path[0][0] == self.start.x and path[0][1] == self.start.y:
            if coordinate == 0:
                x0 = self.start.x
            else:
                x0 = self.start.y
        else:
            if coordinate == 0:
                x0 = self.goal.x
            else:
                x0 = self.goal.y

        new_path = []
        for i in path:
            new_path.append(i[coordinate])
        measurements = np.asarray(new_path)

        kf = KalmanFilter(F=F, H=H, Q=Q, R=R, x0=x0)
        predictions = []

        for idx, z in enumerate(measurements):
            if idx == len(measurements) - 1:
                predictions.append(measurements[idx])
                continue
            predictions.append(np.dot(H, kf.predict())[0][0])
            kf.update(z)

        predictions = list(predictions)
        for i in range(len(path)):
            predictions[i] = float(predictions[i])
            path[i][coordinate] = predictions[i]

        return path

    # For moving nodes more away from obstacles '''NOT USED'''
    def node_moved_by_obstacle(self, path, L_MAX=50,
                               k1=0.05, k2=0.95, MAX_DIST=20):
                          # k1=0.0003, k2=0.9997, MAX_DIST=20):
        for i in range(len(path)):
            # Do not affect starting node
            if i == 0 or i == len(path) - 1:
                continue

            # Chose area by L_MAX where forces made by obstacle are pushing node away
            min_x = int(path[i][0] - L_MAX)
            max_x = int(path[i][0] + L_MAX)
            min_y = int(path[i][1] - L_MAX)
            max_y = int(path[i][1] + L_MAX)
            obstacle_array = self.data_list[(min_y - 1):(max_y + 1), (min_x - 1):(max_x + 1)]
            tmp_array = obstacle_array.flatten()  # makes subarray as 1D array

            # Firstly checks if there is any obstacle at all
            if tmp_array.all():     # if no obstacles continue
                continue

            # Puts node in the center of area
            node = [L_MAX, L_MAX]

            # Loop through each free or taken pixel
            for m, obstacle_list in enumerate(obstacle_array):
                for n, obstacle in enumerate(obstacle_list):
                    if obstacle == True:    # if chosen pixel is free continue
                        continue

                    # Calculates distance from obstacle to node
                    dx = n - node[0]
                    dy = m - node[1]
                    d = math.sqrt(dx ** 2 + dy ** 2)
                    theta = math.atan2(dy, dx)

                    # Distance to move point around
                    delta_l = L_MAX - d
                    dist_xy = -(delta_l * k1) / (k1 + k2)

                    # Wiggled point after forces from obstacles affected it
                    node[0] += dist_xy * math.cos(theta)
                    node[1] += dist_xy * math.sin(theta)

                    # Repeats by each taken pixel

            # Shows node in image coordinates
            node[0] += path[i][0] - L_MAX
            node[1] += path[i][1] - L_MAX

            # If wiggled distance is greater than maximal then move node only by maximal distance
            # and assign it as node of path
            dist_old_new = math.sqrt((node[0] - path[i][0]) ** 2 + (node[1] - path[i][1]) ** 2)
            if dist_old_new > MAX_DIST:
                theta2 = math.atan2((node[0] - path[i][0]), (node[1] - path[i][1]))
                path[i][0] += MAX_DIST * math.cos(theta2)
                path[i][1] += MAX_DIST * math.sin(theta2)
            else:
                path[i][0] = node[0]
                path[i][1] = node[1]
        return path

    # For adding more nodes that are close to obstacles '''(THE OTHER WAY AROUND)'''
    def node_addition(self, path, max_dis=10, min_dis=4):
        node_list = []
        new_path = []

        for i in range(len(path)):
            # Add first connection to list
            if i == 0:
                continue
            node_list.append(path[i - 1])
            node_list.append(path[i])
            point1 = copy.deepcopy(path[i - 1])
            point2 = copy.deepcopy(path[i])

            dx = path[i][0] - path[i - 1][0]
            dy = path[i][1] - path[i - 1][1]
            d = math.sqrt(dx * dx + dy * dy)
            theta = math.atan2(dy, dx)
            j = 1

            while d >= min_dis:
                # If connection too long or close obstacles '''(NOT)''' found
                # then cut connection in half and add new node in between old ones
                if not self.collision_detection_with_distance(p_one=point1, p_two=point2,
                                                              search_radius=self.rob_size) or d > max_dis:
                    point2[0] = point1[0] + d/2 * math.cos(theta)
                    point2[1] = point1[1] + d/2 * math.sin(theta)
                    node_list.insert(j, copy.deepcopy(point2))

                # Else go to next connection or brake if there is no next connection
                else:
                    j += 1
                    if j == len(node_list):
                        break
                point1 = copy.deepcopy(node_list[j - 1])
                point2 = copy. deepcopy(node_list[j])
                dx = point1[0] - point2[0]
                dy = point1[1] - point2[1]
                d = math.sqrt(dx * dx + dy * dy)
            # Removes last sample because it will be first one in next list
            node_list.pop()
            # Add found nodes to new path
            new_path.extend(node_list)
            node_list = []
        # Add final node
        new_path.append(path[i])

        return new_path

    # Deletes nodes in=between till first collision or maximal distance
    def get_shorter_path(self, path):
        new_path = []
        MAX_DIST = self.expand_dis*4

        for i in range(len(path)):
            # Add starting node
            if i == 0:
                new_path.append(path[i])
                j = i
            else:
                # Set maximal distance to expand path
                dx = path[i][0] - path[j][0]
                dy = path[i][1] - path[j][1]
                dist = math.sqrt(dx**2 + dy**2)

                # Add node to new path id distance between nodes is greater than maximal
                if dist >= MAX_DIST and j != 0:
                    j = i - 1
                    new_path.append(path[i - 1])  # last without collision
                    continue

                # Check if there is a collision between nodes
                nearest_node = path[j]
                new_node = path[i]
                if self.collision_detection_with_distance(p_one=nearest_node, p_two=new_node,
                                                          search_radius=self.rob_size*2):
                    continue    # if safe continue

                # If collision then add last point without collision to path
                j = i - 1
                if i - 1 != 0:
                    new_path.append(path[i - 1])    # last without collision
        # Adds last node
        new_path.append(path[-1])
        return new_path

    # Returns path from randomly generated points
    def gen_final_course(self, goalind):
        path = [[self.goal.x, self.goal.y]]

        # Takes goal index and connects it to all its parent nodes till starting point
        while self.node_list[goalind].parent is not None:   # self.start.parent = None
            node = self.node_list[goalind]
            path.append([node.x, node.y])
            goalind = node.parent
        path.append([self.start.x, self.start.y])
        return path

    def drawing(self, rnd=None, pausetime=0.001):
        # Draw newly generated point
        if rnd is not None:
            plt.plot(rnd[0], rnd[1], "^y")

        # Connect two new points
        for node in self.node_list:
            if node.parent is not None:
                plt.plot([node.x, self.node_list[node.parent].x], [node.y, self.node_list[node.parent].y], "-g")

        # Shows start and goal position
        plt.plot(self.start.x, self.start.y, "xr")
        plt.plot(self.goal.x, self.goal.y, "xr")

        plt.axis(self.area_size)

        # plt.grid(True)
        # plt.pause(pausetime)

    def get_nearest_node_idx(self, node_list, rnd):
        dlist = []
        for node in node_list:
            d = (node.x - rnd[0]) ** 2 + (node.y - rnd[1]) ** 2
            dlist.append(d)
        minind = dlist.index(min(dlist))
        return minind

    def collision_detection_with_distance(self, p_one, p_two, search_radius):
        p1 = copy.deepcopy(p_one)
        p2 = copy.deepcopy(p_two)

        deltax = p2[0] - p1[0]
        deltay = p2[1] - p1[1]
        theta = math.atan2(deltay, deltax)

        # Maybe not needed (seems to make no mistakes)
        if deltax == 0:
            return False  # no slope => no normal
        if deltay == 0:
            return False  # division by 0

        # Get coordinates for area to check intensity
        if p1[0] > p2[0]:
            p1, p2 = p2, p1
        if p1[1] > p2[1]:
            p1[1], p2[1] = p2[1], p1[1]

        # Get extension in length by robot size
        p1[0] -= search_radius * math.cos(theta)
        p1[1] -= search_radius * math.sin(theta)
        p2[0] += search_radius * math.cos(theta)
        p2[1] += search_radius * math.sin(theta)

        # Get extension in width by robot size
        theta = -1 / theta
        ax = p1[0] - search_radius * math.cos(theta)    # + math.pi/2)
        by = p1[1] - search_radius * math.sin(theta)    # - math.pi/2)
        cy = p2[1] + search_radius * math.sin(theta)    # - math.pi/2)
        dx = p2[0] + search_radius * math.cos(theta)    # + math.pi/2)

        # Finds min, max values for program to work in all 4 segments
        min_x = min(ax, dx)
        max_x = max(ax, dx)
        min_y = min(by, cy)
        max_y = max(by, cy)

        # Collision detection
        subarray = self.data_list[int(min_y - 1):int(max_y + 2), int(min_x - 1):int(max_x + 2)]
        subarray.flatten()      # makes subarray as 1D array
        if not subarray.all():      # Checks if all values are true
            return False  # collision
        return True  # safe
